<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadosEventosUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estados_eventos_usuarios', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_evento')->default(0);
          $table->integer('id_usuario')->default(0);
          $table->string('imei')->default("");
          $table->string('estado_evento')->default("");
          $table->datetime('fecha')->default(DB::raw('now()'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estados_eventos_usuarios');
    }
}
