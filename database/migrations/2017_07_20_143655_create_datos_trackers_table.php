<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_trackers', function (Blueprint $table) {
          $table->increments('id');
          $table->string('imei')->default("");
          $table->datetime('fecha_servidor')->default(DB::raw('now()'));
          $table->datetime('fecha_tracker')->default(DB::raw('now()'));
          $table->float('latitud')->default(0);
          $table->float('longitud')->default(0);
          $table->float('altitud')->default(0);
          $table->float('angulo')->default(0);
          $table->float('velocidad')->default(0);
          $table->string('parametros')->default("");
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_trackers');
    }
}
