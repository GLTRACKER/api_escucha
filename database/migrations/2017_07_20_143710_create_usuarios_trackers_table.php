<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios_trackers', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_usuario');
          $table->string('imei')->default("");
          $table->string('nombre')->default("");
          $table->string('numero_placa')->default("");
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios_trackers');
    }
}
