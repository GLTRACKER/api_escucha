<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackers', function (Blueprint $table) {
          $table->increments('id');
          $table->string('imei')->default("");
          $table->string('activo')->default("");
          $table->datetime('fecha_activo')->default(DB::raw('now()'));
          $table->integer('id_administrador')->default(0);
          $table->float('latitud')->default(0);
          $table->float('longitud')->default(0);
          $table->float('altitud')->default(0);
          $table->float('angulo')->default(0);
          $table->float('velocidad')->default(0);
          $table->string('parametros')->default("");
          $table->integer('signal_gsm')->default(0);
          $table->integer('signal_gps')->default(0);
          $table->string('tipo_odometro')->default("");
          $table->string('tipo_horas_motor')->default("");
          $table->float('odometro')->default(0);
          $table->integer('horas_motor')->default(0);
          $table->string('fcr')->default("");
          $table->datetime('fecha_servidor')->default(DB::raw('now()'));
          $table->datetime('fecha_tracker')->default(DB::raw('now()'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trackers');
    }
}
