<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosEventosUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_eventos_usuarios', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_evento')->default(0);
          $table->integer('id_usuario')->default(0);
          $table->string('descripcion_evento')->default("");
          $table->string('sistema_notificacion')->default("");
          $table->string('imei')->default("");
          $table->string('nombre_objeto')->default("");
          $table->datetime('fecha_servidor')->default(DB::raw('now()'));
          $table->datetime('fecha_tracker')->default(DB::raw('now()'));
          $table->datetime('fecha_minuto')->default(DB::raw('now()'));
          $table->float('latitud')->default(0);
          $table->float('longitud')->default(0);
          $table->float('altitud')->default(0);
          $table->float('angulo')->default(0);
          $table->float('velocidad')->default(0);
          $table->string('parametros')->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_eventos_usuarios');
    }
}
