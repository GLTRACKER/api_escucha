<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zonas', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_zona')->default(0);
          $table->integer('id_usuario')->default(0);
          $table->string('nombre_zona')->default("");
          $table->string('color_zona')->default("");
          $table->string('visible_zona')->default("");
          $table->string('nombre_visible_zona')->default("");
          $table->string('vertices_zona')->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zonas');
    }
}
