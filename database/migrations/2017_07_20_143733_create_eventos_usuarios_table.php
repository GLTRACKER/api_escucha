<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos_usuarios', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_evento')->default(0);
          $table->integer('id_usuario')->default(0);
          $table->string('tipo')->default("");
          $table->string('nombre')->default("");
          $table->string('activo')->default("");
          $table->string('imei')->default("true");
          $table->string('sistema_notificacion')->default("");
          $table->string('notificacion_email')->default("");
          $table->string('notificacion_direccion_email')->default("");
          $table->string('notificacion_sms')->default("");
          $table->string('comando_enviado')->default("");
          $table->string('valor_verificado')->default("");
          $table->string('tipo_comando')->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos_usuarios');
    }
}
