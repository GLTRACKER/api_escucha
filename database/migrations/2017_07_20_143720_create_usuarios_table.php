<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_usuario')->default(0);
          $table->string('nombre_usuario')->default("");
          $table->string('email')->default("");
          $table->string('encrypted_password')->default("");
          $table->string('activo')->default("true");
          $table->string('email_op')->default("");
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
