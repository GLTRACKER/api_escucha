<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosEventosUsuario extends Model
{
    public $timestamps = false;
}
