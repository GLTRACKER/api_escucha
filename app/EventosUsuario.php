<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventosUsuario extends Model
{
    public $timestamps = false;
}
