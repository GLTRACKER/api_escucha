<?php

namespace App\Http\Controllers;

use App\Zona;
use App\DatosEventosZona;
use App\EventosZonas;
use App\UsuariosTracker;
use App\EstadoUsuarioVehiculoZona;


use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ExcesoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        Log::info('Showing user profile for user: '.$id);
        $GpsData = $request->input('tracker');
        $DataUsuariosTrackers = UsuariosTracker::select('id_usuario','nombre','tipo')->where('imei',$id)->get();
        // dd($DataUsuariosTrackers);
        $eventos = new \ArrayObject();
        $resultado = count($DataUsuariosTrackers);
        if ($resultado) {
            foreach ($DataUsuariosTrackers as $DataUsuariosTracker) {
                $sel_camioneta = 0;
                switch ($DataUsuariosTracker['tipo']) {
                    case 0:
                        $where ="ST_Within(ST_GeomFromText('POINT(".$GpsData['Longitud']." ".$GpsData['Latitud'].")'),zonas.geom) and vel_zona > 5 and id_usuario=".$DataUsuariosTracker['id_usuario'];
                        break;
                    case 1:
                        $sel_camioneta = 10;
                        $where ="ST_Within(ST_GeomFromText('POINT(".$GpsData['Longitud']." ".$GpsData['Latitud'].")'),zonas.geom) and vel_zona > 5 and id_usuario=".$DataUsuariosTracker['id_usuario']." and categoria_zona = 1;";
                        break;
                    case 2:
                        $where ="ST_Within(ST_GeomFromText('POINT(".$GpsData['Longitud']." ".$GpsData['Latitud'].")'),zonas.geom) and vel_zona > 5 and id_usuario=".$DataUsuariosTracker['id_usuario']." and categoria_zona = 1;";
                        break;
                    case 3:
                        $sel_camioneta = 10;
                        $where ="ST_Within(ST_GeomFromText('POINT(".$GpsData['Longitud']." ".$GpsData['Latitud'].")'),zonas.geom) and vel_zona > 5 and id_usuario=".$DataUsuariosTracker['id_usuario']." and categoria_zona = 2;";
                        break;
                    case 4:
                        $where ="ST_Within(ST_GeomFromText('POINT(".$GpsData['Longitud']." ".$GpsData['Latitud'].")'),zonas.geom) and vel_zona > 5 and id_usuario=".$DataUsuariosTracker['id_usuario']." and categoria_zona =2;";
                        break;                        
                }
                $ZonaDatas = Zona::select('id_zona','nombre_zona','vel_zona','vel2_zona','vel3_zona')->whereRaw($where)->get();
                foreach ($ZonaDatas as $ZonaData) {
                    $evento= new \stdClass();
                    $queri='id_usuario = \''.$DataUsuariosTracker['id_usuario'].'\' and id_zona=\''.$ZonaData['id_zona'].'\' and imei= \''.$id.'\'';
                    $EstadoUsuarioVehiculoZonaDatos= DB::table('estado_usuario_vehiculo_zonas')->whereRaw($queri)->first();

                    $resultado = count($EstadoUsuarioVehiculoZonaDatos);
                    if (!$resultado) { 
                        $EstadoUsuarioVehiculoZona = new EstadoUsuarioVehiculoZona;
                        $EstadoUsuarioVehiculoZona->imei = $id;
                        $EstadoUsuarioVehiculoZona->id_zona = $ZonaData['id_zona'];
                        $EstadoUsuarioVehiculoZona->id_usuario = $DataUsuariosTracker['id_usuario'];
                        $EstadoUsuarioVehiculoZona->save();
                        $EstadoUsuarioVehiculoZonaDatos=$EstadoUsuarioVehiculoZona;
                    }
                    if (($ZonaData['vel2_zona']!= null && $ZonaData['vel3_zona']!= null && $ZonaData['vel2_zona']!= 0 && $ZonaData['vel3_zona']!= 0) && ( $ZonaData['vel3_zona']>$ZonaData['vel2_zona']&& $ZonaData['vel2_zona'] >$ZonaData['vel_zona'])) {

                        if (($ZonaData['vel_zona'] + $sel_camioneta) <= $GpsData['Velocidad']) {
                            if($EstadoUsuarioVehiculoZonaDatos->fecha){
                                $diff = strtotime($GpsData['fecha_tracker']) - strtotime($EstadoUsuarioVehiculoZonaDatos->fecha);
                                if ($diff<180) {

                                    $EstadoUsuarioVehiculoZona=EstadoUsuarioVehiculoZona::find($EstadoUsuarioVehiculoZonaDatos->id);
                                    $EstadoUsuarioVehiculoZona->fecha=$GpsData['fecha_tracker'];
                                    $EstadoUsuarioVehiculoZona->save(); 
                              
                                    $DatosEventosZona= new DatosEventosZona;
                                    $DatosEventosZona->latitud = $GpsData['Latitud'];
                                    $DatosEventosZona->velocidad = $GpsData['Velocidad'];
                                    $DatosEventosZona->altitud = $GpsData['Altitud'];
                                    $DatosEventosZona->angulo = $GpsData['Angulo'];
                                    $DatosEventosZona->parametros = $GpsData['Parametros'];
                                    $DatosEventosZona->fecha_tracker = $GpsData['fecha_tracker'];
                                    $DatosEventosZona->fecha_server = $GpsData['fecha_server'];
                                    $DatosEventosZona->longitud = $GpsData['Longitud'];
                                    $DatosEventosZona->id_eventos_zonas = $EstadoUsuarioVehiculoZona['id_eventos_zonas'];
                                    $DatosEventosZona->save();
                                    
                                    $EventoZonas= EventosZonas::find($EstadoUsuarioVehiculoZona['id_evento_zona']);
                                    $EventoZonas->fecha_fin=$GpsData['fecha_tracker'];
                                    $EventoZonas->save();


                                    $diff2 = strtotime($EventoZonas['fecha_fin']) - strtotime($EventoZonas['fecha_inicio']);


                                    if ($diff2>60) {
                                        # code...
                                        // echo "transgrecion";
                                        $d = new \DateTime( $GpsData['fecha_server'] );
                                        $d->modify( '-5 hours' ); //restas 6 horas
                                        $d1 = new \DateTime( $GpsData['fecha_tracker']);
                                        $d1->modify( '-5 hours' ); //restas 6 horas
                                        $d2 = new \DateTime( $EventoZonas['fecha_inicio']);
                                        $d2->modify( '-5 hours' ); //restas 6 horas

                                        $evento->id_usuario=$DataUsuariosTracker['id_usuario'];
                                        $evento->fecha_servidor=$d->format( 'Y-m-d H:i:s' );
                                        $evento->fecha_tracker=$d1->format( 'Y-m-d H:i:s' );
                                        $evento->fecha_inicio=$d2->format( 'Y-m-d H:i:s' );
                                        $evento->id_evento=$EventoZonas['id'];
                                        $evento->nombre_objeto=$DataUsuariosTracker['nombre'];
                                        $evento->imei=$id;
                                        $evento->latitud=$GpsData['Latitud'];
                                        $evento->longitud=$GpsData['Longitud'];
                                        $evento->altitud=$GpsData['Altitud'];
                                        $evento->angulo=$GpsData['Angulo'];
                                        $evento->velocidad=$GpsData['Velocidad'];
                                        $eventos->append($evento);
                                    } else {
                                        # code...
                                        // echo "alertaaaaaa";
                                        $d = new \DateTime( $GpsData['fecha_server'] );
                                        $d->modify( '-5 hours' ); //restas 6 horas
                                        $d1 = new \DateTime( $GpsData['fecha_tracker']);
                                        $d1->modify( '-5 hours' ); //restas 6 horas
                                        $d2 = new \DateTime( $EventoZonas['fecha_inicio']);
                                        $d2->modify( '-5 hours' ); //restas 6 horas

                                        $evento->id_usuario=$DataUsuariosTracker['id_usuario'];
                                        $evento->fecha_servidor=$d->format('Y-m-d H:i:s');
                                        $evento->fecha_tracker=$d1->format('Y-m-d H:i:s');
                                        $evento->fecha_inicio=$d2->format('Y-m-d H:i:s');
                                        $evento->id_evento=$EventoZonas['id'];
                                        $evento->nombre_objeto=$DataUsuariosTracker['nombre'];
                                        $evento->imei=$id;
                                        $evento->latitud=$GpsData['Latitud'];
                                        $evento->longitud=$GpsData['Longitud'];
                                        $evento->altitud=$GpsData['Altitud'];
                                        $evento->angulo=$GpsData['Angulo'];
                                        $evento->velocidad=$GpsData['Velocidad'];
                                        $eventos->append($evento);
                                    }

                                } else {
                                    # code...
                                    $EventoZonas= new EventosZonas;
                                    $EventoZonas->fecha_inicio=$GpsData['fecha_tracker'];
                                    $EventoZonas->fecha_fin=null;
                                    $EventoZonas->id_zona=$ZonaData['id_zona'];
                                    $EventoZonas->imei=$id;
                                    $EventoZonas->save();                                    

                                    $EstadoUsuarioVehiculoZonaDatos=EstadoUsuarioVehiculoZona::find($EstadoUsuarioVehiculoZonaDatos->id);
                                    $EstadoUsuarioVehiculoZonaDatos->fecha=$GpsData['fecha_tracker'];
                                    $EstadoUsuarioVehiculoZonaDatos->id_evento_zona=$EventoZonas['id'];
                                    $EstadoUsuarioVehiculoZonaDatos->save(); 

                                    $DatosEventosZona= new DatosEventosZona;
                                    $DatosEventosZona->latitud = $GpsData['Latitud'];
                                    $DatosEventosZona->velocidad = $GpsData['Velocidad'];
                                    $DatosEventosZona->altitud = $GpsData['Altitud'];
                                    $DatosEventosZona->angulo = $GpsData['Angulo'];
                                    $DatosEventosZona->parametros = $GpsData['Parametros'];
                                    $DatosEventosZona->fecha_tracker = $GpsData['fecha_tracker'];
                                    $DatosEventosZona->fecha_server = $GpsData['fecha_server'];
                                    $DatosEventosZona->longitud = $GpsData['Longitud'];
                                    $DatosEventosZona->id_eventos_zonas = $EstadoUsuarioVehiculoZonaDatos['id_evento_zona'];
                                    $DatosEventosZona->save();                                    
                                }                                
                            }else{

                                $EventoZonas= new EventosZonas;
                                $EventoZonas->fecha_inicio=$GpsData['fecha_tracker'];
                                $EventoZonas->fecha_fin=null;
                                $EventoZonas->id_zona=$ZonaData['id_zona'];
                                $EventoZonas->imei=$id;
                                $EventoZonas->save();

                                $EstadoUsuarioVehiculoZonaDatos=EstadoUsuarioVehiculoZona::find($EstadoUsuarioVehiculoZonaDatos->id);
                                $EstadoUsuarioVehiculoZonaDatos->fecha=$GpsData['fecha_tracker'];
                                $EstadoUsuarioVehiculoZonaDatos->id_evento_zona=$EventoZonas['id'];
                                $EstadoUsuarioVehiculoZonaDatos->save(); 

                                $DatosEventosZona= new DatosEventosZona;
                                $DatosEventosZona->latitud = $GpsData['Latitud'];
                                $DatosEventosZona->velocidad = $GpsData['Velocidad'];
                                $DatosEventosZona->altitud = $GpsData['Altitud'];
                                $DatosEventosZona->angulo = $GpsData['Angulo'];
                                $DatosEventosZona->parametros = $GpsData['Parametros'];
                                $DatosEventosZona->fecha_tracker = $GpsData['fecha_tracker'];
                                $DatosEventosZona->fecha_server = $GpsData['fecha_server'];
                                $DatosEventosZona->longitud = $GpsData['Longitud'];
                                $DatosEventosZona->id_eventos_zonas = $EstadoUsuarioVehiculoZonaDatos['id_evento_zona'];
                                $DatosEventosZona->save();                             
                                
                            }
                        } else {
                            # code...
                            if($EstadoUsuarioVehiculoZonaDatos->fecha){
                               $EstadoUsuarioVehiculoZonas=EstadoUsuarioVehiculoZona::find($EstadoUsuarioVehiculoZonaDatos->id);
                                $EstadoUsuarioVehiculoZonas->fecha=null;
                                $EstadoUsuarioVehiculoZonas->save();  
                            }                     
                        }
                        
                        
                    } else {
                        # code...
                    } 

                    $diff = strtotime('2009-10-05 18:17:24') - strtotime('2009-10-05 18:07:13');                    
                    // echo "diferencia en segundos";
                    // echo $diff;                    

                     #'{usuario_tracker['id_usuario']} AND id_zona = #{zonas['id_zona']} AND imei = '#{post_imei}'
                    // dd($eventos);
                }
            }
            # code...
            // echo "contenido".$resultado;
                       
        } else {
            // echo "vacio".$resultado;
            # code...
        }
        return response()->json([
            'mensaje' => $eventos,
            'state' => 'CA'
        ]);
        
        // ->where('type', $type);
        // select('id_usuario','nombre','tipo');
        // ->where("imei",  $id);
        // "SELECT  id_zona, nombre_zona, vel_zona, vel2_zona, vel3_zona FROM  zonas where ST_Within(ST_GeomFromText('POINT("+post_longitud+" "+post_latitud+")'),zonas.geom) and vel_zona > 5 and id_usuario="+usuario_tracker.id_usuario.to_s
        // dd($repond);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
