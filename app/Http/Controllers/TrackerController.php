<?php

namespace App\Http\Controllers;

use App\Tracker;
use App\DatosTracker;
use App\Dtwomonth;
use App\TramasError;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;

class TrackerController extends Controller
{

    protected $json_mensaje;

    public function update_imei(Request $request, $id)
    {
      //dd($request);
      $status = true;

      $this->json_mensaje = array();
      $json_prueba = "";
      $global_alertas_exceso = "false";

      $json_tracker_status = "false";
      $json_eventos = array();
      //$json_hash_evento = {};

      if($request['tracker']){
        $post_imei = $id;

        $buscar_tracker = Tracker::where('imei', $post_imei)->first();

        if($buscar_tracker){

          $post_signal_gps = (string)$request['tracker']['signal_gps'];
          $post_signal_gsm = (string)$request['tracker']['signal_gsm'];
          $post_parametros = (string)$request['tracker']['Parametros'];
          $post_velocidad = (float)$request['tracker']['velocidad'];
          $post_angulo = (float)$request['tracker']['angulo'];
          $post_altitud = (float)$request['tracker']['altitud'];
          $post_longitud = (float)$request['tracker']['longitud'];
          $post_latitud = (float)$request['tracker']['latitud'];
          $post_fecha_tracker = $request['tracker']['fecha_tracker'];
          $post_fecha_servidor = $request['tracker']['fecha_servidor'];
          $post_eventos = (string)$request['tracker']['eventos'];

          if ($post_latitud != 0 && $post_longitud != 0){
            if (($post_latitud < 13 && $post_latitud > -56) && ($post_longitud > -82 && $post_longitud < -35)){
              $diferencia_fecha_params = strtotime($post_fecha_tracker) - strtotime($post_fecha_servidor);

              $fecha_hoy = new DateTime();
              $fecha_hoy = date_format($fecha_hoy,"Y/m/d H:i:s");

              $segundos = strtotime($fecha_hoy) - strtotime($post_fecha_tracker);

              $tiempo = $segundos / 60;
              $tiempo = $tiempo / 60;
              $tiempo = $tiempo / 24;
              $dias_tracker = $tiempo;

              //echo $dias_tracker;

              if (($diferencia_fecha_params >= 0) && ($dias_tracker<=60)) {
                $post_velocidad = round($post_velocidad, 1);
                $post_altitud = round($post_altitud, 1);
                //dd($buscar_tracker->fecha_tracker);
                $diferencia_fecha_tracker = strtotime($buscar_tracker->fecha_tracker) - strtotime($post_fecha_tracker);
                //echo 'neuva diferencia';
                //echo $diferencia_fecha_tracker;

                if ($diferencia_fecha_tracker >= 0){
                  $buscar_tracker->signal_gps = $post_signal_gps;
                  $buscar_tracker->signal_gsm = $post_signal_gsm;
                  $buscar_tracker->velocidad = $post_velocidad;
                  $buscar_tracker->angulo = $post_angulo;
                  $buscar_tracker->altitud = $post_altitud;
                  $buscar_tracker->longitud = $post_longitud;
                  $buscar_tracker->latitud = $post_latitud;
                  $buscar_tracker->fecha_tracker = $post_fecha_tracker;
                  $buscar_tracker->fecha_servidor = $post_fecha_servidor;
                  $buscar_tracker->parametros = $post_parametros;
                  $buscar_tracker->save();
                  if ($buscar_tracker) {
                    $json_tracker_status = "true";
                  }
                }

                $datos_tracker = new DatosTracker;
                //dd($datos_tracker);
                $datos_tracker->imei = $post_imei;
                $datos_tracker->fecha_servidor = $post_fecha_servidor;
                $datos_tracker->fecha_tracker = $post_fecha_tracker;
                $datos_tracker->latitud = $post_latitud;
                $datos_tracker->longitud = $post_longitud;
                $datos_tracker->altitud = $post_altitud;
                $datos_tracker->angulo = $post_angulo;
                $datos_tracker->velocidad = (int)$post_velocidad;
                $datos_tracker->parametros = $post_parametros;
                $guardar_datos_tracker = $datos_tracker->save();
                if ($guardar_datos_tracker) {

                  $formato_fecha_historial = date_format(new DateTime($post_fecha_tracker),"Y/m/d H:i");
                  $formato_fecha_historial = (string)$formato_fecha_historial.":00";
                  $dtwomonth = $this->guardar_dtwomonth($datos_tracker, $formato_fecha_historial);
                } else {
                  array_push($this->json_mensaje, "No se guardó en datos tracker");
                }
              }
            } else {
              $error_message = "Trama fuera de rango de fechas";
              $trama_error = $this->guardar_tramaerror($request, $post_imei, $error_message);
            }
          } else {
            $error_message = "Trama incorrecta";
            $trama_error = $this->guardar_tramaerror($request, $post_imei, $error_message);
          }
        } else {
          $error_message = "No existe el IMEI";
          $trama_error = $this->guardar_tramaerror($request, $post_imei, $error_message);
        }
      } else {
        array_push($this->json_mensaje, "No existen datos");
      }
      return 'ok';
    }

    public function guardar_dtwomonth($trama_tracker, $fecha_historial) {
      $dtwomonth = new Dtwomonth;
      $dtwomonth->imei = $trama_tracker->imei;
      $dtwomonth->fecha_servidor = $trama_tracker->fecha_servidor;
      $dtwomonth->fecha_tracker = $trama_tracker->fecha_tracker;
      $dtwomonth->fecha_historial = $fecha_historial;
      $dtwomonth->latitud = $trama_tracker->latitud;
      $dtwomonth->longitud = $trama_tracker->longitud;
      $dtwomonth->altitud = $trama_tracker->altitud;
      $dtwomonth->angulo = $trama_tracker->angulo;
      $dtwomonth->velocidad = $trama_tracker->velocidad;
      $dtwomonth->parametros = $trama_tracker->parametros;
      $guardar = $dtwomonth->save();
      if ($guardar) {
        //array_push($json_mensaje, "Guardado en Dtwomonth");
        //dd($json_mensaje);
      } else {
        array_push($this->json_mensaje, "Error en guardado en Dtwomonth");
      }
    }

    public function guardar_tramaerror($trama_tracker, $imei, $observacion) {
      //dd($trama_tracker);
      $tramaerror = new TramasError;
      $tramaerror->imei = $imei;
      $tramaerror->fecha_servidor = $trama_tracker['tracker']['fecha_servidor'];
      $tramaerror->fecha_tracker = $trama_tracker['tracker']['fecha_tracker'];
      $tramaerror->latitud = $trama_tracker['tracker']['latitud'];
      $tramaerror->longitud = $trama_tracker['tracker']['longitud'];
      $tramaerror->altitud = $trama_tracker['tracker']['altitud'];
      $tramaerror->angulo = $trama_tracker['tracker']['angulo'];
      $tramaerror->velocidad = $trama_tracker['tracker']['velocidad'];
      $tramaerror->parametros = $trama_tracker['tracker']['Parametros'];
      $tramaerror->observacion = $observacion;
      $guardar = $tramaerror->save();
      if ($guardar) {
        //array_push($this->json_mensaje, "Guardado en TramasError");
        //dd($this->json_mensaje);
        //echo "Guarddo en tramaserror";
      } else {
        array_push($this->json_mensaje, "Error en guardado en TramasError");
      }
    }
}
